package SoundX;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class Sound {
	
	public void getSound(float f, int time) throws LineUnavailableException {
		byte[] buf = new byte[1];
		AudioFormat af = new AudioFormat((float) 44100, 8, 1, true, false);
		SourceDataLine sdl = AudioSystem.getSourceDataLine(af);
		sdl.open();
		sdl.start();
		for (int i = 0; i < 10000 * (float) time / 10000; i++) {
			double angle = i / ((float) f / 440) * 2.0 * Math.PI;
			buf[0] = (byte) (Math.sin(angle) * 120);
			sdl.write(buf, 0, 1);
		}

		sdl.drain();
		sdl.stop();
		sdl.close();
	}
}