package SoundX;

public enum Frequencies {
	  c3(130.81f),
      cS3(138.59f),
      d3(146.83f),
      dS3(155.56f),
      e3(164.81f),
      f3(174.61f),
      fS3(185.0f),
      g3(196.0f),
      gS3(207.65f),
      a3(220.0f),
      aS3(233.08f),
      b3(246.94f),
      c4(261.63f),
      cS4(277.18f),
      d4(293.66f),
      dS4(311.13f),
      e4(329.63f),
      f4(349.23f),
      fS4(369.99f),
      g4(392.0f),
      gS4(415.3f),
      a4(440.0f),
      aS4(466.16f),
      b4(493.88f),
      c5(523.25f),
      cS5(554.37f),
      d5(587.33f),
      dS5(622.25f),
      e5(659.25f),
      f5(698.46f),
      fS5(739.99f),
      g5(783.99f),
      gS5(830.61f),
      a5(880.0f),
      aS5(962.33f),
      b5(987.77f);
  	  private float value;
      private Frequencies(float value) {
          this.value = value;
  }
      public float getFrequency(){
    	  return value;
      }

}
