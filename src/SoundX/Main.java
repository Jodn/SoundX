package SoundX;

import java.util.Random;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class Main {

	public static void main(String[] args) throws LineUnavailableException, InterruptedException {
		Sound s = new Sound();
		int dummyInt = 0;

		Random toneInt = new Random();

		while (dummyInt > -1) {
			int fnum = toneInt.nextInt(Frequencies.values().length);
			System.out.print("toneInt; ");
			System.out.println(fnum);

			s.getSound(Frequencies.values()[fnum].getFrequency(), 10000);

			dummyInt++;
			Thread.sleep(1000);
		}

	}

}
